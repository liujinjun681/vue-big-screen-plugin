const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false,
  productionSourceMap: false,
  devServer: {
    port: 8003,
    open: true,
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      ["/proxy_python"]: {
        target: `http://192.168.1.100:8000/`,
        pathRewrite: {
          ["^" + "/proxy_python"]: "",
        },
      },
      ["/proxy_weather/"]: {
        target: `https://apia.aidioute.cn/weather/`,
        pathRewrite: {
          ["^/.*"]: "index.php?location_type=1&lat=30.58&lng=114.03",
        },
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        additionalData: `@import "~@/assets/scss/mixins.scss";`,
      },
    },
  },
});
