import { createRouter, createWebHashHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Index",
    component: () => import("../views/hdindex/index.vue"),
  },
  {
    path: "/index",
    name: "HdIndex",
    component: () => import("../views/index/index.vue"),
  },
  {
    path: "/histroy",
    name: "histroy",
    component: () => import("../views/hdindex/ammeter.vue"),
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
