import { ModuleInfo } from '.././index.d'

// 星期
export const WEEK = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
export const videoInfo = [
      {
        pos: "itema3",
        name:'1',
        show:true,
        url: "ws://localhost:9005/rtsp/2/?url=rtsp://admin:TZZBKX@192.168.6.236:554/h264/ch1/main/av_stream",
      },
      {
        pos: "itema4", name:'2', show:true,
        url: "ws://localhost:9005/rtsp/2/?url=rtsp://admin:VYYPAX@192.168.6.130:554/h264/ch1/main/av_stream",
      },
      {
        pos: "itemb2",name:'3', show:true,
        url: "ws://localhost:9005/rtsp/2/?url=rtsp://admin:VYYPAX@192.168.6.130:554/h264/ch1/main/av_stream",
      },
      {
        pos: "itemb3",name:'4', show:true,
        url: "ws://localhost:9005/rtsp/2/?url=rtsp://admin:VYYPAX@192.168.6.130:554/h264/ch1/main/av_stream",
      },
      {
        pos: "itemc2",name:'5', show:true,
        url: "ws://localhost:9005/rtsp/2/?url=rtsp://admin:VYYPAX@192.168.6.130:554/h264/ch1/main/av_stream",
      },
    ]

// 主题名称与副标题名称
export const title = '信息化看板'
export const subtitle = ['HD', '', '']

export const moduleInfo: ModuleInfo = [
  // 中间的几个模块
  {
    name: '任务通过率',
    icon: 'icon-chart-bar',
  },
  {
    name: '地图数据',
    icon: 'icon-tongji4',
  },
  {
    name: '产品销售渠道分析',
    icon: 'icon-align-left',
  },
  {
    name: '任务完成排行榜',
    icon: 'icon-zhibiao2',
  },
  // 底部两个模块
  {
    name: '数据统计图',
    icon: 'icon-vector',
  },
  {
    name: '工单修复以及满意度统计图',
    icon: 'icon-fenxi7',
  },
]
