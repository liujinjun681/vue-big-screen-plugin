import { defineComponent, ref, watch, shallowReactive } from "vue";

// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true,
  },
} as const;

// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {
    // 定义 ref
    const chartRef = ref();
    // 配置项
    let options = shallowReactive({ tooltip: null, series: null });

    const height =  props.cdata.height || "300px";
    const width =  props.cdata.width || "300px";

    watch(
      () => props.cdata,
      (val: any) => {
        options = {
          tooltip: {
            formatter: "{a} <br/>{b} : {c}",
          },
          series: [
            {
              type: "gauge",
              data: props.cdata.seriesData,
              splitNumber: 5,
              radius: '60%',
              detail: {
                fontSize: 20,
                valueAnimation: true,
                formatter: "{value}",
                offsetCenter: [0, '60%'],
              },
              title: {
              color: 'white',
              fontSize: 20,
              offsetCenter: ['0', '100%'],
            },
              axisLabel: {
                distance: -20,
                color: "#999",
                fontSize: 20,
              },
              axisLine: {
                distance: -30,
                roundCap: true,
                lineStyle: {
                  width: 18,
                },
              },
              splitLine: {
                distance: -32,
              },
              axisTick: {
                distance: -30,
                style: {
                  stroke: "#fff",
                },
              },
              itemStyle: {
                color: "#58D9F9",
                shadowColor: "rgba(0,138,255,0.45)",
                shadowBlur: 10,
                shadowOffsetX: 2,
                shadowOffsetY: 2,
              },
              progress: {
                show: true,
                roundCap: true,
                width: 18,
              },
              pointer: {
                icon: "path://M2090.36389,615.30999 L2090.36389,615.30999 C2091.48372,615.30999 2092.40383,616.194028 2092.44859,617.312956 L2096.90698,728.755929 C2097.05155,732.369577 2094.2393,735.416212 2090.62566,735.56078 C2090.53845,735.564269 2090.45117,735.566014 2090.36389,735.566014 L2090.36389,735.566014 C2086.74736,735.566014 2083.81557,732.63423 2083.81557,729.017692 C2083.81557,728.930412 2083.81732,728.84314 2083.82081,728.755929 L2088.2792,617.312956 C2088.32396,616.194028 2089.24407,615.30999 2090.36389,615.30999 Z",
                length: "75%",
                width: 16,
                offsetCenter: [0, "5%"],
              },
              animationCurve: "easeInOutBack",
            },
          ],
        };
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options);
        }
      },
      {
        immediate: true,
        deep: true,
      }
    );

    return () => { 
      return (
        <div>
          <echart
            ref={chartRef}
            options={options}
            height={height}
            width={width}
          />
        </div>
      );
    };
  },
});
