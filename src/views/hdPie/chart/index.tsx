import { defineComponent, onUnmounted, reactive } from "vue";
import Draw from "./draw";

export default defineComponent({
  components: {
    Draw,
  },
  setup() {
    const intervalInstance = null;
    const cdata = reactive({
      xData: ["数据1", "数据2", "数据3", "数据4", "数据5", "数据6"],
      seriesData: [{ name: "回火炉开机率", value: 55 }],
      width:'450px',
    });
    // intervalInstance = setInterval(() => {
    //   const data = cdata.seriesData;
    //   cdata.seriesData = data.map((e) => {
    //     return { value: Math.round(100*Math.random()), name: e.name };
    //   });
    // }, 1000);

    onUnmounted(() => {
      clearInterval(intervalInstance);
    });
    return () => {
      return (
        <div>
          <Draw cdata={cdata} />
        </div>
      );
    };
  },
});
