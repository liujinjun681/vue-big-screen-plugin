import { defineComponent, ref, watch, shallowReactive } from "vue";
import { getDict } from "@/utils/index";
// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true,
  },

} as const;

// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {
    // 定义 ref
    const chartRef = ref();
    // 配置项
    let options = shallowReactive({ tooltip: null, series: null });

    const height = props.cdata.height || "300px";
    const width = props.cdata.width || "300px";

    watch(
      () => props.cdata,
      (val: any) => {
        const oridata = props.cdata.data;
        const detail = props.cdata.detail
        const splitNumber = oridata.options ?oridata.options.length -1: 7;
        const max = oridata.options ?oridata.options.length -1: oridata.max;
        const min =  oridata.min || 0;
        let color = "#74d129"
        if(oridata.max && (oridata.value >= oridata.max || oridata.value <= oridata.min )) color = "#d12929"
        if(detail && (detail.value >= detail.max || detail.value <= detail.min )) color = "#d12929"
       
        const colorArr = ["#293ed1" ,"#298ed1", "#29d185", "#74d129", "#d1c429", "#d18629", "#d12929"]
        

        options = {
          tooltip: {
            formatter: "{a} <br/>{b} : {c}",
          },
          series: [
            {
              type: "gauge",
              data: props.cdata.seriesData,
              splitNumber,
              radius: '60%',
              detail: {
                valueAnimation: true,
                color,
                fontSize: 18,
                offsetCenter: [0, '60%'],
                formatter: function (value) {
                  if(detail) return detail.value + detail.unit
                  return getDict(value, oridata.options) + (oridata.unit || '');
                },
              },
              title: {
                color: "white",
                fontSize: 20,
                offsetCenter: ["0", "100%"],
              },
              min,
              max,
              axisLabel: {
                distance: -10,
                color: "#999",
                rotate: "tangential",
                formatter: function (v) {
                  if(!oridata.options) {
                    if(max > 10)
                    return Math.round(v)
                    else return v.toFixed(2)
                  }
                  return getDict(v, oridata.options);
                },
              },
              axisLine: {
                distance: -20,
                roundCap: true,
                lineStyle: {
                  width: 8,
                  color: [
                    [1/splitNumber, colorArr[0]],
                    [2/splitNumber, colorArr[Math.round(7/splitNumber)]],
                    [3/splitNumber, colorArr[Math.round(14/splitNumber)]],
                    [4/splitNumber, colorArr[Math.round(21/splitNumber)]],
                    [5/splitNumber, colorArr[Math.round(28/splitNumber)]],
                    [6/splitNumber, colorArr[Math.round(35/splitNumber)]],
                    [7/splitNumber, colorArr[Math.round(42/splitNumber)]],                
                  ],
                },
              },
              splitLine: {
                distance: -25,
              },
              axisTick: {
                distance: -20,
                style: {
                  stroke: "#fff",
                },
              },
              itemStyle: {
                color: "#58D9F9",
                shadowColor: "rgba(0,138,255,0.45)",
                shadowBlur: 10,
                shadowOffsetX: 2,
                shadowOffsetY: 2,
              },
              // progress: {
              //   show: true,
              //   roundCap: true,
              //   width: 8,
              // },

              animationCurve: "easeInOutBack",
            },
          ],
        };
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options);
        }
      },
      {
        immediate: true,
        deep: true,
      }
    );

    return () => {
      return (
        <div>
          <echart
            ref={chartRef}
            options={options}
            height={height}
            width={width}
          />
        </div>
      );
    };
  },
});
