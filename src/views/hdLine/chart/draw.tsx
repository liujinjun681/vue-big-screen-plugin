import { defineComponent, watch, ref } from 'vue';
// 声明类型
const PropsType = {
  cdata: {
    type: Object,
    require: true
  }
} as const

// 定义主体
export default defineComponent({
  props: PropsType,
  setup(props) {
    // 定义 ref
    const chartRef = ref()
    // 定义颜色
    const colorList = {
      linearYtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#f5b44d"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      linearPtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "#9d87f5"
          },
          {
            offset: 1,
            color: "#5131d5"
          }
        ]
      },
      linearGtoB: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#43dfa2"
          },
          {
            offset: 1,
            color: "#31dfaf"
          }
        ]
      },
      linearBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 1,
        y2: 0,
        colorStops: [
          {
            offset: 0,
            color: "#1c98e8"
          },
          {
            offset: 1,
            color: "#28f8de"
          }
        ]
      },
      areaBtoG: {
        type: "linear",
        x: 0,
        y: 0,
        x2: 0,
        y2: 1,
        colorStops: [
          {
            offset: 0,
            color: "rgba(35,184,210,.2)"
          },
          {
            offset: 1,
            color: "rgba(35,184,210,0)"
          }
        ]
      }
    }
    // 配置项
    let options ={}

    // 监听
    watch(
      () => props.cdata,
      (val: any) => {
        options = {
          title: {
            text: "1号机",
            textStyle: {
              color: "#D3D6DD",
              fontSize: 24,
              fontWeight: "normal"
            },
            subtextStyle: {
              color: "#fff",
              fontSize: 16
            },
            top: 50,
            left: 80
          },
          legend:{
            show:true,
          },
          tooltip: {
            trigger: "item"
          },
          grid: {
            left: 60,
            right: 60,
            bottom: '15%',
            top: "15%"
          },
          xAxis: {
            type: "category",
            position: "bottom",
            axisLine: true,
            axisLabel: {
              color: "rgba(255,255,255,.8)",
              fontSize: 12
            },
          },
          // 下方Y轴
          yAxis: {
            nameLocation: "end",
            nameGap: 24,
            nameTextStyle: {
              color: "rgba(255,255,255,.5)",
              fontSize: 14
            },
            splitNumber: 4,

            axisLine: {
              lineStyle: {
                opacity: 0
              }
            },
            splitLine: {
              show: true,
              lineStyle: {
                color: "#fff",
                opacity: 0.1
              }
            },
            axisLabel: {
              color: "rgba(255,255,255,.8)",
              fontSize: 12
            }
          },
          series: [
            {
              name: "SV",
              type: "line",
              smooth: true,
              symbol: "emptyCircle",
              symbolSize: 8,
              itemStyle: {
                normal: {
                  color: "#fff"
                }
              },
              lineStyle: {
                normal: {
                  color: colorList.linearPtoG,
                  width: 3
                }
              },
              areaStyle: {
                normal: {
                  color: colorList.areaBtoG
                }
              },
              data: val.seriesData?.actual_SV,
              lineSmooth: true,
              markLine: {
                silent: true,
                data: [
                  {
                    type: "average",
                    name: "平均值"
                  }
                ],
                precision: 0,
                label: {
                  normal: {
                    formatter: "平均值: \n {c}"
                  }
                },
                lineStyle: {
                  normal: {
                    color: "rgba(248,211,81,.7)"
                  }
                }
              },
              tooltip: {
                position: "top",
                formatter: "{c} ℃",
                backgroundColor: "rgba(28,152,232,.2)",
                padding: 6
              }
            },
            {
              name: "PV",
              type: "line",
              smooth: true,
              symbol: "emptyCircle",
              symbolSize: 8,
              itemStyle: {
                normal: {
                  color: "#fff"
                }
              },
              lineStyle: {
                normal: {
                  color: colorList.linearBtoG,
                  width: 3
                }
              },
              areaStyle: {
                normal: {
                  color: colorList.areaBtoG
                }
              },
              data: val.seriesData?.actual_PV,
              lineSmooth: true,
              tooltip: {
                position: "top",
                formatter: "{c} ℃",
                backgroundColor: "rgba(28,152,232,.2)",
                padding: 6
              }
            },
            {
              name: "setting_SV",
              type: "line",
              smooth: true,
              symbol: "emptyCircle",
              symbolSize: 8,
              itemStyle: {
                normal: {
                  color: "#fff"
                }
              },
              lineStyle: {
                normal: {
                  color: colorList.linearYtoG,
                  width: 3
                }
              },
              areaStyle: {
                normal: {
                  color: colorList.areaBtoG
                }
              },
              data: val.seriesData?.setting_SV,
              lineSmooth: true,
              tooltip: {
                position: "top",
                formatter: "{c} ℃",
                backgroundColor: "rgba(28,152,232,.2)",
                padding: 6
              }
            },
          ]
        }
        // 手动触发更新
        if (chartRef.value) {
          // 通过初始化参数打入数据
          chartRef.value.initChart(options)
        }
      },
      {
        immediate: true,
        deep: true
      }
    )

    return () => {
      const height = "300px"
      const width = "100%"

      return <div>
        <echart ref={chartRef} height={height} width={width} />
      </div>
    }
  }
})

