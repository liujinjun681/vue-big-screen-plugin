import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import dataV from "@jiaminghi/data-view";
// 引入全局css
import "./assets/scss/style.scss";
import "animate.css/animate.min.css"; //引入
// import 'animate.css' //引入

// 引入图表（所有图标见 icon 目录下的 demo_index.html）
import "./assets/icon/iconfont.css";
// 引入 全局注册组件
import PublicComponent from "@/components/componentInstall";
import "@/utils/jsmpeg.min";
import moment from "moment";
moment.locale("zh-cn");
moment.updateLocale("zh-cn", {
  week: {
    dow: 1, // 星期的第一天是星期一
    doy: 7, // 年份的第一周必须包含1月1日 (7 + 1 - 1)
  },
});
moment.defaultFormat = "YYYY-MM-DD HH:mm:ss";

const app = createApp(App);
app.config.globalProperties.moment = moment;
app.config.globalProperties.$moment = moment;
app.use(PublicComponent);
app.use(dataV);
app.use(store);
app.use(router);
app.mount("#app");
