import { createStore } from "vuex";
const json = require("./get_glf.json");
import { getDict } from "@/utils/index";
export default createStore({
  state: {
    config: json.config,
    chartData: json.data,
    chartData2: [
      {
        key: "zt",
        value: 0,
        val: 250,
        title: "电泳车间告警",
        label: "电泳车间",
        isWarn: false,
        warnMsg: "",
        options: [
          {
            key: 0,
            value: "液位正常",
          },
          {
            key: 1,
            value: "液位偏高",
          },
          {
            key: 2,
            value: "液位偏低",
          },
          {
            key: 3,
            value: "网络异常",
          },
        ],
      },
    ],
  },
  mutations: {
    setChartData(state, val) {
      state.chartData = val;
    },
    setDycjData(state, val) {
      state.chartData2 = val;
    },
    setSccwData(state, val) {
      val.forEach((element) => {
        const item = state.chartData.find((o) => element.key === o.key);
        if (item) {
          item.value = element.value;
        } else {
          state.chartData.push(element);
        }
      });
    },
    setConfig(state, val) {
      state.config = val;
    },
  },
  actions: {
    getChartData(context) {
      fetch("/proxy_python/get_glf/")
        .then((res) => {
          return res.json();
        })
        .then((res) => {
          if (res.data) context.commit("setChartData", res.data);
          if (res.config) context.commit("setConfig", res.config);
          // fetch("/proxy_python/get_glf_01/")
          //   .then((res) => {
          //     return res.json();
          //   })
          //   .then((res) => {
          //     if (res.data) context.commit("setSccwData", res.data);
          //   });
        });
      // fetch("/proxy_python/get_yewei/")
      //   .then((res) => {
      //     return res.json();
      //   })
      //   .then((res) => {
      //     if (res.data) context.commit("setDycjData", res.data);
      //   });
    },
    setConfig(context, config) {
      //delete config.config.is_alarm;
      config.data.forEach((o) => {
        delete o.disabled;
        delete o.isWarn;
        delete o.warnMsg;
      });

      const raw = JSON.stringify(config);
      const requestOptions = {
        method: "POST",
        body: raw,
      };
      fetch("/proxy_python/update_glf_json/", requestOptions)
        .then((response) => response.json())
        .then((result) => {
          console.log(result);
          context.dispatch("getChartData");
        })
        .catch((error) => console.log("error", error));
    },
  },
  getters: {
    getParamsByKey: (state) => (keyname) => {
      return state.chartData.find((o) => o.key === keyname);
    },
    getParamsByKey2: (state) => (keyname) => {
      return state.chartData2.find((o) => o.key === keyname);
    },
    getWarnInfo: (state) => {
      const glylzt = state.chartData.find((o) => o.key === "gltlzt_060D");
      const glyl = state.chartData.find((o) => o.key === "zqyl_0115");
      const pywd = state.chartData.find((o) => o.key === "pywd_0101");

      const data = state.chartData.map((o) => {
        o.isWarn = false;
        o.warnMsg = "";
        o.title = "锅炉房告警";
        if (o.key === "yxzt_0602") {
          //运行状态报警
          if (o.value == "3") {
            o.isWarn = true;
            o.warnMsg = "锅炉运行报警！！！";
          }
        }
        if (o.key === "bjyl_0509") {
          //锅炉压力报警设定
          glyl.max = o.value;
          //glyl.min = o.value - 0.12;
          // glyl.options = [
          //   {
          //     key: glyl.min,
          //     value: "",
          //   },
          //   {
          //     key: 0.94,
          //     value: "正常",
          //   },
          //   {
          //     key: glyl.max,
          //     value: "超压",
          //   },
          // ];
          if (glyl.value >= glyl.max) {
            glylzt.isWarn = true;
            glylzt.warnMsg =
              "锅炉蒸汽超压!!! 当前锅炉蒸汽压力：" + glyl.value + glyl.unit;
            glylzt.value = 4;
          } else {
            glylzt.value = 1;
          }
        }

        if (o.key === "pybjwd_0500") {
          pywd.max = o.value;
        }

        // 通用报警
        if (o.value == "aaaa") {
          switch (o.key) {
            case "cybj_0773": // 锅炉压力相关报警
            case "zqylcgqgz_0715":
              o.warnMsg =
                o.label + "!!! 当前锅炉蒸汽压力：" + glyl.value + glyl.unit;
              break;
            case "pybjwd_0500": // 排烟相关报警
            case "pywdgyj_0770":
              o.warnMsg =
                o.label + "!!! 当前锅炉排烟温度：" + pywd.value + pywd.unit;
              break;
            default:
              o.warnMsg = o.label + "!!!";
          }
          o.isWarn = true;
        }
        // 水槽报警信息
        if (o.key === "clz_0000") {
          if (o.value >= o.max) {
            o.warnMsg = "水槽水位过高!!! 当前水位：" + o.value + o.unit;
            o.isWarn = true;
          }
          if (o.value <= o.min) {
            o.warnMsg = "水槽水位过低!!! 当前水位：" + o.value + o.unit;
            o.isWarn = true;
          }
        }
        // 电泳车间报警信息
        if (o.key === "zt") {
          if (o.value != 0) {
            o.title = "电泳车间告警";
            o.isWarn = true;
            o.warnMsg = o.label + getDict(o.value, o.options) + "!!!";
          }
        }
        return o;
      });
      return data.filter((o) => o.isWarn && o.isAlarm);
    },
  },
  modules: {},
});
